package com.sensei;

public class PersonEligiblePremService implements ParsonCheck{
	
	@Override
	public int getMalePercent(int premium) {
		return premium * 2/100;
	}

	@Override
	public int getFemalePercent(int premium) {
		return premium * 2/100;
	}

	@Override
	public int getOtherPercent(int premium) {
		return premium * 2/100;
	}

	@Override
	public int getBelow18(int premium) {
		return 0;
	}

	@Override
	public int get18and25(int premium) {
		return premium * 10 / 100;
	}

	@Override
	public int get25and30(int premium) {
		return premium * 10 / 100;
	}

	@Override
	public int get30and35(int premium) {
		return premium * 10 / 100;
	}

	@Override
	public int get35and40(int premium) {
		return premium * 10 / 100;
	}

	@Override
	public int getAbove40(int premium) {
		return premium * 20/ 100;
	}
	
	@Override
	public int getHypertensionPercent(int premium) {
		return premium*1/100;
	}

	@Override
	public int getBloodPressurePercent(int premium) {
		return premium*1/100;
	}

	@Override
	public int getBloodSugarPercent(int premium) {
		return premium*1/100;
	}

	@Override
	public int getOverweightPercent(int premium) {
		return premium*1/100;
	}
	
	@Override
	public int getSmokingPercent(int premium) {
		return premium * 3 / 100;
	}

	@Override
	public int getAlcoholPercent(int premium) {
		return premium * 3 / 100;
	}

	@Override
	public int getDailyExercisePercent(int premium) {
		return premium * 3 / 100;
	}

	@Override
	public int getDrugsPercent(int premium) {
		return premium * 3 / 100;
	}
	
	
	@Override
	public double getPersonGenderPremiumAmount(Person per, int BasePremium) {
		if (per.getGender().getCode() == 1) {
			per.GenderPremiumAmount = BasePremium * 2 / 100;
		}
		System.out.println("Gender Premium Amount : (Increase 2%) "
				+ per.GenderPremiumAmount);
		return per.GenderPremiumAmount;
	}

	@Override
	public double getPersonAgePremiumAmount(Person per, int premium) {
		if (per.getAge() < 18) {
			per.AgePremiumAmount = getBelow18(premium);
		} else if (per.getAge() >= 18 && per.getAge() <= 25) {
			per.AgePremiumAmount = get18and25(premium);
		} else if (per.getAge() >= 25 && per.getAge() <= 30) {
			per.AgePremiumAmount = get25and30(premium);
		} else if (per.getAge() >= 30 && per.getAge() <= 35) {
			per.AgePremiumAmount = get30and35(premium);
		} else if (per.getAge() >= 30 && per.getAge() <= 35) {
			per.AgePremiumAmount = get35and40(premium);
		} else {
			per.AgePremiumAmount = getAbove40(premium);
		}
		
		System.out.println("Age Premium Amount : " + per.AgePremiumAmount);
		return per.AgePremiumAmount;
	}

	@Override
	public double getPersonHealthPremiumAmount(Person per, int premium) {
		if (per.isHyperTension()) {
			per.healthPremiumAmount += getHypertensionPercent(premium);
		}
		if (per.isBloodPressure()) {
			per.healthPremiumAmount += getBloodPressurePercent(premium);
		}
		if (per.isBloodSugar()) {
			per.healthPremiumAmount += getBloodSugarPercent(premium);
		}
		if (per.isOverWeight()) {
			per.healthPremiumAmount += getOverweightPercent(premium);
		}
		System.out.println("Current Health Premium Amount Person : "+ per.healthPremiumAmount);
		return per.healthPremiumAmount;
		
	}
	
	@Override
	public double getPersonHabitsPremiumAmount(Person per, int premium) {
		if (per.isSmoking()) {
			per.habitsPremiumAmount += getSmokingPercent(premium);
		}
		if (per.isAlcohol()) {
			per.habitsPremiumAmount += getAlcoholPercent(premium);
		}
		if (per.isExcercise()) {
			per.habitsPremiumAmount -= getDailyExercisePercent(premium);
		}
		if (per.isDrugs()) {
			per.habitsPremiumAmount += getDrugsPercent(premium);
		}
		System.out
				.println("Habits Premium Amount : "
						+ per.habitsPremiumAmount);
		return per.habitsPremiumAmount;
	}
	

}
