package com.sensei;

import java.util.Scanner;
public class GomesHealthInsurance {
	
	public static final int BASE_PREMIUM_FOR_PERSION = 5000;

	public static double InsuranceCalculateForPerson(Person p) {
		
		PersonEligiblePremService person = new PersonEligiblePremService();		
		 
		return BASE_PREMIUM_FOR_PERSION
				+ person.getPersonAgePremiumAmount(p, BASE_PREMIUM_FOR_PERSION)
				+ person.getPersonGenderPremiumAmount(p, BASE_PREMIUM_FOR_PERSION)
				+ person.getPersonHabitsPremiumAmount(p, BASE_PREMIUM_FOR_PERSION)
				+ person.getPersonHealthPremiumAmount(p, BASE_PREMIUM_FOR_PERSION);
		}

	
	public static void main(String[] args) {
		
		Person person = new Person();
		
		Scanner scan = new Scanner(System.in);
		try {
			String gender = "";
			System.out.println("Enter the person name :");
			String firstName =scan.next();
			
			person.setName(firstName);
			
			System.out.println("Enter the Gender like (Male for M ,Femalle for F  ,Other O)");
			gender = scan.next();
			
			if (gender.equalsIgnoreCase("M")) {
				person.setGender(Gender.MALE);
			} else if (gender.equalsIgnoreCase("F")) {
				person.setGender(Gender.FEMALE);
			} else {
				person.setGender(Gender.OTHER);
			}
			
			System.out.println("Enter Age:");
			String age=scan.next();
			person.setAge(Integer.parseInt(age));
			
			System.out.println(" Health Related issue ");
			System.out.println("HyperTension(Yes/NO):");
			
			person.setHyperTension("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("BloodPressure(Yes/No):");
			person.setBloodPressure("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("BloodSugar(Yes/No):");
			person.setBloodSugar("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("OverWeight(Yes/No):");
			person.setOverWeight("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);

			System.out.println("Person  Habit Details ");
			System.out.println("Smoke(Yes/No):");
			person.setSmoking("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("Alcohol(Yes/No):");
			person.setAlcohol("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("Daily Exercise(Yes/No):");
			person.setAlcohol("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);
			System.out.println("Drugs(Yes/No):");
			person.setDrugs("Yes".equalsIgnoreCase(scan.next()) ? true
					: false);

			System.out.println("Health Insurance for Mr."
					+ person.getName() + ": Rs." +InsuranceCalculateForPerson(person));

				
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}
		
}
