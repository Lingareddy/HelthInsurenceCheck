package com.sensei;

public enum Gender {
	MALE(1), FEMALE(2), OTHER(3);

	private int flag;

	private Gender(int flag) {
		this.flag = flag;
	}

	public int getCode() {
		return this.flag;
	}
}
