package com.sensei;

public interface ParsonCheck {
	
	//gender details
		int getMalePercent(int premium);
		int getFemalePercent(int premium);
		int getOtherPercent(int premium);
		double getPersonGenderPremiumAmount(Person per, int BasePremium);
		
		//Age details
		int getBelow18(int premium);
		int get18and25(int premium);
		int get25and30(int premium);
		int get30and35(int premium);
		int get35and40(int premium);
		int getAbove40(int premium);
		double getPersonAgePremiumAmount(Person per, int premium);
		
		//Health details
		int getHypertensionPercent(int premium);
		int getBloodPressurePercent(int premium);
		int getBloodSugarPercent(int premium);
		int getOverweightPercent(int premium);
		double getPersonHealthPremiumAmount(Person per, int premium);
		
		//Habits details
		int getSmokingPercent(int premium);
		int getAlcoholPercent(int premium);
		int getDailyExercisePercent(int premium);
		int getDrugsPercent(int premium);
		double getPersonHabitsPremiumAmount(Person per, int premium);
		

}
